# Team: PVirie #

Tech Jam Challenge 2017

### Prerequisites ###

* Python 2.7.x
* Tensorflow 1.2

### Running guideline ###

* Extract data into the 'data' directory
* Run each of the task, say 'python task_1.py --rate 0.0001 --max_training_steps 10', to train the model for task 1.
* or use 'python task_1.py --load --gen' to load the trained parameters and generate test labels.
* use 'python task_1.py -h' to display options for the task, same goes for the other task files.

### Project structure ###

* 'artifacts' contains saved parameter files, and generated label files.
* 'data' contains data for each task in their own folders.
	** 1
	** 2
	** 3
	** 4
	** 5
* 'src' contains the shared network model 'scanner.py' and the utility file 'util.py'
* task_*.py task files, each file refers to 'scanner.py' and 'util.py' inside the src folder
* 'README.md' this file

