import tensorflow as tf
import argparse
import numpy as np
# import matplotlib.pyplot as plt
import src.scanner as scanner
import os
import csv
import src.util as util
from datetime import datetime
from operator import itemgetter

stime = datetime.utcfromtimestamp(0)
dir_path = os.path.dirname(os.path.realpath(__file__))

zip_codes = []
merchant_ids = []
merchant_categories = []

billing_stat = util.get_start_stat()
issue_stat = util.get_start_stat()
exp_stat = util.get_start_stat()
limit_stat = util.get_start_stat()
prev_stat = util.get_start_stat()

income_stat = util.get_start_stat()
age_stat = util.get_start_stat()
credit_stat = util.get_start_stat()

stamp_stat = util.get_start_stat()
amount_stat = util.get_start_stat()


customers = {}
print "reading customer file..."
with open(dir_path + "/data/1/tj_01_creditcard_customer.csv", 'r') as file:
    reader = csv.reader(file, delimiter=',')
    next(reader)
    for row in reader:
        person = []
        person.append(row[0])  # customer id
        person.append(int(row[1]))  # income
        try:
            person.append(int(row[2]))  # age
        except ValueError:
            person.append(0)
        person.append(util.check_and_update_list(zip_codes, row[3]))  # zipcode
        person.append(float(row[4]))  # credits
        customers[row[0]] = person

cards = {}
print "reading credit card file..."
with open(dir_path + "/data/1/tj_01_creditcard_card.csv", 'r') as file:
    reader = csv.reader(file, delimiter=',')
    next(reader)
    for row in reader:
        card = []
        # card.append(row[0])  # card id
        card.append(util.collect_statistics(billing_stat, int(row[1])))  # billing cycle
        card.append(util.collect_statistics(issue_stat, (datetime.strptime(row[4], '%Y-%m-%d %H:%M:%S') - stime).total_seconds()))  # issue date
        card.append(util.collect_statistics(exp_stat, (datetime.strptime(util.card_expiry_to_full_time(row[5]), '%Y/%m/%d') - stime).total_seconds()))  # exp
        card.append(util.collect_statistics(limit_stat, float(row[6])))  # limit
        card.append(util.collect_statistics(prev_stat, float(row[7])))  # prev limit
        customer = customers[row[3]]
        card.append(util.collect_statistics(income_stat, customer[1]))  # income
        card.append(util.collect_statistics(age_stat, customer[2]))  # age
        # card.append(customer[3])  # zipcode assume not relevant
        card.append(util.collect_statistics(credit_stat, customer[4]))  # credits
        cards[row[0]] = card


def sort_transaction(transactions):
    return sorted(transactions, key=itemgetter(0))


def sequence_summarize(sorted_transactions):
    out = []
    temp = [0, 0, 0]
    for t in sorted_transactions:
        if t[0] == temp[0] and t[2] == temp[2]:
            temp[1] = temp[1] + t[1]
        else:
            out.append(temp)
            temp = [t[0], t[1], t[2]]
    out.append(temp)
    return out[1:]


card_transactions = {}
print "reading transaction file..."
with open(dir_path + "/data/1/tj_01_creditcard_transaction.csv", 'r') as file:
    reader = csv.reader(file, delimiter=',')
    next(reader)
    for row in reader:
        transaction = []
        # transaction.append(row[0])  # card id
        transaction.append(util.collect_statistics(stamp_stat, (datetime.strptime(row[1][0:11] + util.zeropad_2_digits(row[2]) + row[1][13:], '%Y-%m-%d %H:%M:%S') - stime).total_seconds()))
        transaction.append(util.collect_statistics(amount_stat, float(row[3])))  # amount
        transaction.append(util.check_and_update_list(merchant_categories, row[4]))  # merchant code
        # transaction.append(util.check_and_update_list(merchant_ids, row[5]))  # merchant id assume not relevant

        if row[0] in card_transactions:
            card_transactions[row[0]].append(transaction)
        else:
            card_transactions[row[0]] = [transaction]


def normalize_card(card):
    out = np.zeros((8), dtype=np.float32)
    out[0] = util.normalize(card[0], billing_stat)
    out[1] = util.normalize(card[1], issue_stat)
    out[2] = util.normalize(card[2], exp_stat)
    out[3] = util.normalize(card[3], limit_stat)
    out[4] = util.normalize(card[4], prev_stat)
    out[5] = util.normalize(card[5], income_stat)
    out[6] = util.normalize(card[6], age_stat)
    out[7] = util.normalize(card[7], credit_stat)

    return out


def normalize_transactions(trans):
    for i in xrange(len(trans)):
        trans[i][0] = util.normalize(trans[i][0], stamp_stat)
        trans[i][1] = util.normalize(trans[i][1], amount_stat)

    return trans


def batch_to_one_hot(array_like, bin_sizes):
    out = np.zeros((len(array_like), np.sum(bin_sizes)), dtype=np.float32)
    for i in xrange(len(array_like)):
        last = 0
        for j in xrange(len(bin_sizes)):
            next_ = (last + bin_sizes[j])
            out[i, last:next_] = util.to_one_hot(array_like[i][j], bin_sizes[j])
            last = next_
    return out


def prep_data(tuples):
    profiles = []
    trans = []
    labels = []

    for tuple in tuples:
        profiles.append(normalize_card(tuple[0]))
        trans.append(batch_to_one_hot(normalize_transactions(tuple[1]), [1, 1, len(merchant_categories)]))
        # print len(tuple[1])
        labels.append(1.0 if tuple[2] else 0)
    # print "==="
    return (np.stack(profiles), np.stack(trans), np.stack(labels))


parser = argparse.ArgumentParser()
parser.add_argument("--rate", help="learning rate", type=float)
parser.add_argument("--load", help="load weight", action="store_true")
parser.add_argument("--gen", help="gen predictions", action="store_true")
parser.add_argument("--max_training_length", help="length of training set (define batch size)", type=int)
parser.add_argument("--max_training_steps", help="number of iterations", type=int)
args = parser.parse_args()

if __name__ == '__main__':

    learning_rate = 0.001 if not args.rate else args.rate
    max_training_length = 1000 if not args.max_training_length else args.max_training_length
    total_iterations = 10 if not args.max_training_steps else args.max_training_steps

    print "-----------------------"
    print "Load: ", args.load
    print "learning rate: ", learning_rate
    print "max sequence length: ", max_training_length
    print "total iterations: ", total_iterations
    print "-----------------------"

    print len(zip_codes), len(merchant_ids), len(merchant_categories)

    counts = []
    for card, value in card_transactions.iteritems():
        counts.append(len(value))

    # plt.plot(xrange(len(counts)), counts)
    # plt.ylabel('amount')
    # plt.show()

    batch_by_len = {}
    print "reading training file..."
    with open(dir_path + "/data/1/tj_01_training.csv", 'r') as file:
        reader = csv.reader(file, delimiter=',')
        # next(reader)
        for row in reader:
            card_id = row[0]
            flag = row[1]
            card = cards[card_id]
            transactions = sequence_summarize(sort_transaction(card_transactions[card_id]))

            if len(transactions) in batch_by_len:
                batch_by_len[len(transactions)].append((card, transactions, flag is '1'))
            else:
                batch_by_len[len(transactions)] = [(card, transactions, flag is '1')]

    count_so_far = 0
    training_batches = []
    batch = []
    for key in batch_by_len:
        # print "items in batch by len:", len(batch_by_len[key]), key
        for item in batch_by_len[key]:
            batch.append(item)
            if count_so_far + key < max_training_length:
                count_so_far = count_so_far + key
            else:
                training_batches.append(prep_data(batch))
                del batch[:]
                count_so_far = 0
        if count_so_far > 0:
            training_batches.append(prep_data(batch))
            del batch[:]
            count_so_far = 0

    print billing_stat
    print issue_stat
    print exp_stat
    print limit_stat
    print prev_stat
    print income_stat
    print age_stat
    print credit_stat
    print stamp_stat
    print amount_stat

    print training_batches[20][1]

    sess = tf.Session()
    model = scanner.Network(sess, learning_rate, training_batches[0][1].shape[2], training_batches[0][0].shape[1], 64)
    sess.run(tf.global_variables_initializer())

    if args.load:
        print "loading..."
        model.load("./artifacts/p1")

    if args.gen:
        print "reading test file..."
        results = []
        with open(dir_path + "/data/1/tj_01_test.csv", 'r') as file:
            reader = csv.reader(file, delimiter=',')
            # next(reader)
            for row in reader:
                card_id = row[0]
                card = cards[card_id]
                transactions = sequence_summarize(sort_transaction(card_transactions[card_id]))
                results.append(model.scan(prep_data([(card, transactions, True)]))[0])

        with open('./artifacts/1.txt', 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=',')
            # writer.writerow(['RowId', 'Location'])
            for i in xrange(len(results)):
                writer.writerow([1 if results[i] > 0.5 else 0])
    else:
        model.train(training_batches, "./artifacts/p1", total_iterations)

        correct_count = 0
        total_count = 0
        for b in training_batches:
            results = model.scan(b)
            for i in xrange(results.shape[0]):
                if results[i] > 0.5 and b[2][i] > 0.5:
                    correct_count = correct_count + 1
                elif results[i] < 0.5 and b[2][i] < 0.5:
                    correct_count = correct_count + 1
                total_count = total_count + 1

        print "prediction on training set:", correct_count * 100 / total_count
