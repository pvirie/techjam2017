import tensorflow as tf
import argparse
import numpy as np
# import matplotlib.pyplot as plt
import src.scanner as scanner
import os
import csv
import src.util as util
from datetime import datetime
from operator import itemgetter

stime = datetime.utcfromtimestamp(0)
dir_path = os.path.dirname(os.path.realpath(__file__))

customer_types = []
units = []
operation_types = []


open_stat = util.get_start_stat()
recent_stat = util.get_start_stat()
dormant_stat = util.get_start_stat()
# freq_stat = util.get_start_stat()
# interest_stat = util.get_start_stat()

amount_stat = util.get_start_stat()
date_stat = util.get_start_stat()

accounts = {}
print "reading account file..."
with open(dir_path + "/data/3/tj_03_account_info.csv", 'r') as file:
    reader = csv.reader(file, delimiter=',')
    next(reader)
    for row in reader:
        account = []
        # account.append(row[0])  # account id
        account.append(util.collect_statistics(open_stat, (datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S') - stime).total_seconds()))  # open date
        account.append(util.check_and_update_list(customer_types, row[3]))  # customer
        account.append(util.collect_statistics(recent_stat, (datetime.strptime(row[4], '%Y-%m-%d %H:%M:%S') - stime).total_seconds()))  # recent date
        account.append(util.collect_statistics(dormant_stat, float(row[5])))  # dormant
        # account.append(util.collect_statistics(freq_stat, float(row[6])))  # freq
        account.append(util.check_and_update_list(units, row[7]))  # unit
        # account.append(util.collect_statistics(interest_stat, float(row[8])))  # interest
        accounts[row[0]] = account


def sort_transaction(transactions):
    return sorted(transactions, key=itemgetter(1))


def sequence_summarize(sorted_transactions):
    out = []
    temp = [0, 0, 0]
    for t in sorted_transactions:
        if t[1] == temp[1] and t[2] == temp[2]:
            temp[0] = temp[0] + t[0]
        else:
            out.append(temp)
            temp = [t[0], t[1], t[2]]
    out.append(temp)
    return out[1:]


account_transactions = {}
print "reading transaction file..."
with open(dir_path + "/data/3/tj_03_deposit_txn.csv", 'r') as file:
    reader = csv.reader(file, delimiter=',')
    next(reader)
    for row in reader:
        transaction = []
        # transaction.append(row[0])  # account id
        transaction.append(util.collect_statistics(amount_stat, float(row[2])))  # amount
        transaction.append(util.collect_statistics(date_stat, (datetime.strptime(row[3], '%Y-%m-%d %H:%M:%S') - stime).total_seconds()))
        transaction.append(util.check_and_update_list(operation_types, row[5]))  # type

        if row[0] in account_transactions:
            account_transactions[row[0]].append(transaction)
        else:
            account_transactions[row[0]] = [transaction]


def normalize_account(account, bin_sizes):
    out = np.zeros((np.sum(bin_sizes)), dtype=np.float32)
    out[0] = util.normalize(account[0], open_stat)
    out[1 + account[1]] = 1
    out[1 + bin_sizes[1]] = util.normalize(account[2], recent_stat)
    out[2 + bin_sizes[1]] = util.normalize(account[3], dormant_stat)
    out[3 + bin_sizes[1] + account[4]] = 1

    return out


def normalize_transactions(trans):
    for i in xrange(len(trans)):
        trans[i][0] = util.normalize(trans[i][0], amount_stat)
        trans[i][1] = util.normalize(trans[i][1], date_stat)

    return trans


def batch_to_one_hot(array_like, bin_sizes):
    out = np.zeros((len(array_like), np.sum(bin_sizes)), dtype=np.float32)
    for i in xrange(len(array_like)):
        last = 0
        for j in xrange(len(bin_sizes)):
            next_ = (last + bin_sizes[j])
            out[i, last:next_] = util.to_one_hot(array_like[i][j], bin_sizes[j])
            last = next_
    return out


def prep_data(tuples):
    profiles = []
    trans = []
    labels = []

    for tuple in tuples:
        profiles.append(normalize_account(tuple[0], [1, len(customer_types), 1, 1, len(units)]))
        trans.append(batch_to_one_hot(normalize_transactions(tuple[1]), [1, 1, len(operation_types)]))
        # print len(tuple[1])
        labels.append(1.0 if tuple[2] else 0)
    # print "==="
    return (np.stack(profiles), np.stack(trans), np.stack(labels))


parser = argparse.ArgumentParser()
parser.add_argument("--rate", help="learning rate", type=float)
parser.add_argument("--load", help="load weight", action="store_true")
parser.add_argument("--gen", help="gen predictions", action="store_true")
parser.add_argument("--max_training_length", help="length of training set (define batch size)", type=int)
parser.add_argument("--max_training_steps", help="number of iterations", type=int)
args = parser.parse_args()

if __name__ == '__main__':

    learning_rate = 0.001 if not args.rate else args.rate
    max_training_length = 5000 if not args.max_training_length else args.max_training_length
    total_iterations = 10 if not args.max_training_steps else args.max_training_steps

    print "-----------------------"
    print "Load: ", args.load
    print "learning rate: ", learning_rate
    print "max sequence length: ", max_training_length
    print "total iterations: ", total_iterations
    print "-----------------------"

    print len(customer_types), len(units), len(operation_types)

    counts = []
    for acc, value in account_transactions.iteritems():
        counts.append(len(value))

    # plt.plot(xrange(len(counts)), counts)
    # plt.ylabel('amount')
    # plt.show()

    batch_by_len = {}
    print "reading training file..."
    with open(dir_path + "/data/3/tj_03_training.csv", 'r') as file:
        reader = csv.reader(file, delimiter=',')
        # next(reader)
        for row in reader:
            acc_id = row[0]
            flag = row[1]
            account = accounts[acc_id]
            transactions = sequence_summarize(sort_transaction(account_transactions[acc_id]))

            if len(transactions) in batch_by_len:
                batch_by_len[len(transactions)].append((account, transactions, flag == '1'))
            else:
                batch_by_len[len(transactions)] = [(account, transactions, flag == '1')]

    count_so_far = 0
    training_batches = []
    batch = []
    for key in batch_by_len:
        # print "items in batch by len:", len(batch_by_len[key]), key
        for item in batch_by_len[key]:
            batch.append(item)
            if count_so_far + key < max_training_length:
                count_so_far = count_so_far + key
            else:
                if count_so_far < max_training_length * 2:
                    training_batches.append(prep_data(batch))
                del batch[:]
                count_so_far = 0
        if count_so_far > 0:
            if count_so_far < max_training_length * 2:
                training_batches.append(prep_data(batch))
            del batch[:]
            count_so_far = 0

    print training_batches[20][2]

    sess = tf.Session()
    model = scanner.Network(sess, learning_rate, training_batches[0][1].shape[2], training_batches[0][0].shape[1], 100)
    sess.run(tf.global_variables_initializer())

    if args.load:
        print "loading..."
        model.load("./artifacts/p3")

    if args.gen:
        print "reading test file..."
        results = []
        with open(dir_path + "/data/3/tj_03_test.csv", 'r') as file:
            reader = csv.reader(file, delimiter=',')
            # next(reader)
            for row in reader:
                acc_id = row[0]
                account = accounts[acc_id]
                transactions = sequence_summarize(sort_transaction(account_transactions[acc_id]))
                results.append(model.scan(prep_data([(account, transactions, True)]))[0])

        with open('./artifacts/3.txt', 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=',')
            # writer.writerow(['RowId', 'Location'])
            for i in xrange(len(results)):
                writer.writerow([1 if results[i] > 0.5 else 0])
    else:
        model.train(training_batches, "./artifacts/p3", total_iterations)

        correct_count = 0
        total_count = 0
        for b in training_batches:
            results = model.scan(b)
            for i in xrange(results.shape[0]):
                if results[i] > 0.5 and b[2][i] > 0.5:
                    correct_count = correct_count + 1
                elif results[i] < 0.5 and b[2][i] < 0.5:
                    correct_count = correct_count + 1
                total_count = total_count + 1

        print "prediction on training set:", correct_count * 100 / total_count
