import tensorflow as tf
import argparse
import numpy as np
# import matplotlib.pyplot as plt
import src.scanner as scanner
import os
import csv
import src.util as util
from datetime import datetime
from operator import itemgetter

stime = datetime.utcfromtimestamp(0)
dir_path = os.path.dirname(os.path.realpath(__file__))

operation_types = []

issue_stat = util.get_start_stat()
close_stat = util.get_start_stat()
dormant_stat = util.get_start_stat()

amount_stat = util.get_start_stat()

accounts = {}
print "reading credit card file..."
with open(dir_path + "/data/4/account_info.csv", 'r') as file:
    reader = csv.reader(file, delimiter=',')
    next(reader)
    for row in reader:
        account = []
        # account.append(row[0])  # account id
        account.append(util.collect_statistics(issue_stat, (datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S.0') - stime).total_seconds()))  # open date
        if row[3] == 'NULL':
            account.append(0)
        else:
            account.append(util.collect_statistics(close_stat, (datetime.strptime(row[3], '%Y-%m-%d %H:%M:%S.0') - stime).total_seconds()))  # close date
        account.append(util.collect_statistics(dormant_stat, float(row[4])))  # dormant
        accounts[row[0]] = account


def sort_transaction(transactions):
    return sorted(transactions, key=itemgetter(1))


def sequence_summarize(sorted_transactions):
    out = []
    temp = [0, 0, 0]
    for t in sorted_transactions:
        if t[1] == temp[1] and t[2] == temp[2]:
            temp[0] = temp[0] + t[0]
        else:
            out.append(temp)
            temp = [t[0], t[1], t[2]]
    out.append(temp)
    return out[1:]


account_transactions = {}
print "reading transaction file..."
with open(dir_path + "/data/4/account_transaction.csv", 'r') as file:
    reader = csv.reader(file, delimiter=',')
    next(reader)
    for row in reader:
        transaction = []
        # transaction.append(row[0])  # account id
        transaction.append(util.collect_statistics(amount_stat, float(row[2])))  # amount
        transaction.append(datetime.strptime(row[3], '%Y-%m-%d %H:%M:%S.0'))
        transaction.append(util.check_and_update_list(operation_types, row[5]))  # type

        if row[0] in account_transactions:
            account_transactions[row[0]].append(transaction)
        else:
            account_transactions[row[0]] = [transaction]


def normalize_account(account):
    out = np.zeros((3), dtype=np.float32)
    out[0] = util.normalize(account[0], issue_stat)
    out[1] = util.normalize(account[1], close_stat)
    out[2] = util.normalize(account[2], dormant_stat)
    return out


def normalize_transactions(trans):
    for i in xrange(len(trans)):
        trans[i][0] = util.normalize(trans[i][0], amount_stat)
    return trans


def date_to_one_hot(date):
    return np.concatenate([util.to_one_hot(date.month - 1, 12), util.to_one_hot(date.day - 1, 31), util.to_one_hot(date.weekday(), 7)])


def batch_to_one_hot(array_like):
    out = np.zeros((len(array_like), 1 + 50 + 2), dtype=np.float32)
    for i in xrange(len(array_like)):
        out[i, 0] = array_like[i][0]
        out[i, 1:51] = date_to_one_hot(array_like[i][1])
        out[i, 51:] = util.to_one_hot(array_like[i][2], 2)
    return out


def prep_data(tuples):
    profiles = []
    trans = []
    labels = []

    for tuple in tuples:
        profiles.append(normalize_account(tuple[0]))
        trans.append(batch_to_one_hot(normalize_transactions(tuple[1])))
        # print len(tuple[1])
        labels.append(1.0 if tuple[2] else 0)
    # print "==="
    return (np.stack(profiles), np.stack(trans), np.stack(labels))


parser = argparse.ArgumentParser()
parser.add_argument("--rate", help="learning rate", type=float)
parser.add_argument("--load", help="load weight", action="store_true")
parser.add_argument("--gen", help="gen predictions", action="store_true")
parser.add_argument("--max_training_length", help="length of training set (define batch size)", type=int)
parser.add_argument("--max_training_steps", help="number of iterations", type=int)
args = parser.parse_args()

if __name__ == '__main__':

    learning_rate = 0.001 if not args.rate else args.rate
    max_training_length = 1000 if not args.max_training_length else args.max_training_length
    total_iterations = 10 if not args.max_training_steps else args.max_training_steps

    print "-----------------------"
    print "Load: ", args.load
    print "learning rate: ", learning_rate
    print "max sequence length: ", max_training_length
    print "total iterations: ", total_iterations
    print "-----------------------"

    print len(operation_types)

    counts = []
    for acc, value in account_transactions.iteritems():
        counts.append(len(value))

    # plt.plot(xrange(len(counts)), counts)
    # plt.ylabel('amount')
    # plt.show()

    batch_by_len = {}
    print "reading training file..."
    with open(dir_path + "/data/4/tj_04_training.csv", 'r') as file:
        reader = csv.reader(file, delimiter=',')
        # next(reader)
        for row in reader:
            acc_id = row[0]
            flag = row[1]
            account = accounts[acc_id]
            transactions = account_transactions[acc_id]

            if len(transactions) in batch_by_len:
                batch_by_len[len(transactions)].append((account, transactions, flag == 'sa'))
            else:
                batch_by_len[len(transactions)] = [(account, transactions, flag == 'sa')]

    count_so_far = 0
    training_batches = []
    batch = []
    for key in batch_by_len:
        # print "items in batch by len:", len(batch_by_len[key]), key
        for item in batch_by_len[key]:
            batch.append(item)
            if count_so_far + key < max_training_length:
                count_so_far = count_so_far + key
            else:
                training_batches.append(prep_data(batch))
                del batch[:]
                count_so_far = 0
        if count_so_far > 0:
            training_batches.append(prep_data(batch))
            del batch[:]
            count_so_far = 0

    print issue_stat
    print close_stat
    print dormant_stat
    print amount_stat

    print training_batches[20][2]

    sess = tf.Session()
    model = scanner.Network(sess, learning_rate, training_batches[0][1].shape[2], training_batches[0][0].shape[1], 32)
    sess.run(tf.global_variables_initializer())

    if args.load:
        print "loading..."
        model.load("./artifacts/p4")

    if args.gen:
        print "reading test file..."
        results = []
        with open(dir_path + "/data/4/tj_04_test.csv", 'r') as file:
            reader = csv.reader(file, delimiter=',')
            # next(reader)
            for row in reader:
                acc_id = row[0]
                account = accounts[acc_id]
                transactions = account_transactions[acc_id]
                results.append(model.scan(prep_data([(account, transactions, True)]))[0])

        with open('./artifacts/4.txt', 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=',')
            # writer.writerow(['RowId', 'Location'])
            for i in xrange(len(results)):
                writer.writerow(['sa' if results[i] > 0.5 else 'ca'])
    else:
        model.train(training_batches, "./artifacts/p4", total_iterations)

    correct_count = 0
    total_count = 0
    for b in training_batches:
        results = model.scan(b)
        for i in xrange(results.shape[0]):
            if results[i] > 0.5 and b[2][i] > 0.5:
                correct_count = correct_count + 1
            elif results[i] < 0.5 and b[2][i] < 0.5:
                correct_count = correct_count + 1
            total_count = total_count + 1

    print "prediction on training set:", correct_count * 100 / total_count
